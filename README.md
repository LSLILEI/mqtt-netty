# mqtt-netty

#### 介绍
一个纯净版MQTT服务，适合学习或集成到生产项目中去使用
基于springboot2.1.5,netty4.1.53.Final 实现mqtt服务<br/>
实现功能<br/>
&nbsp;&nbsp;1.服务端发布主题消息，所用订阅该主题客户端可接收到数据。<br/>
&nbsp;&nbsp;2.qos类型级别校验，默认使用qos1级别。<br/>
&nbsp;&nbsp;3.账号密码校验，客户端id校验。<br/>
&nbsp;&nbsp;4.发布主题是否一致校验。<br/>
&nbsp;&nbsp;5.屏蔽springboot自带的web端口，如果有需要可删除application.yml<br/>
spring:<br/>
&nbsp;&nbsp;&nbsp;&nbsp;main:<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;web-application-type: none<br/>
&nbsp;&nbsp;6.windowsMQTT客户端安装包:mqttfx-1.7.1-windows-x64.exe

